Grafana dashboard setup:

* Query options
  * Max data points: 600 if your data collection interval is 1min, adjust on your needs
* Visualization: Graph
* Display:
  * Bars: off
  * Lines: off
  * Points: off
* Series overrides
  * `max`
    * Color: Yellow
    * Fill gradient: 2
    * Fill below to: min
  * `mean`
    * Color: Green
    * Fill gradient: 2
    * Fill below to: min
  * `min`
    * Color: Blue
    * Lines: true
  * `packet_loss%`:
    * Color: Red
    * Y-axis: 2
    * Lines: true
    * Line width: 2
    * Staircase line: true
* Axes
  * Left Y
    * Unit: ms
    * Y-Min: 0
  * Right Y
    * Unit: Percent
    * Y-Min: 0
    * Y-Max: 100
    
References: 
* https://peter.run/blog/2019-07-28-visualising-latency-variance-in-grafana-in-2019/
* https://github.com/grafana/influxdb-flux-datasource/issues/42#issuecomment-532402207
