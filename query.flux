bucket_name = "telegraf"
src_hostname = "hostname"
dst_url = "8.8.8.8"
outputColumns = ["_field", "_time", "_value"]

rawLatencyQuery = from(bucket: bucket_name)
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "ping")
  |> filter(fn: (r) => r["_field"] == "average_response_ms")
  |> filter(fn: (r) => r["host"] == src_hostname)
  |> filter(fn: (r) => r["url"] == dst_url)

meanLatencyRet = rawLatencyQuery
  |> map(fn: (r) => ({_value: r._value, _time: r._time, _field: "mean"}))
  |> keep(columns: outputColumns)
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")

maxLatencyRet = rawLatencyQuery
  |> map(fn: (r) => ({_value: r._value, _time: r._time, _field: "max"}))
  |> keep(columns: outputColumns)
  |> aggregateWindow(every: v.windowPeriod, fn: max, createEmpty: false)
  |> yield(name: "max")

minLatencyRet = rawLatencyQuery
  |> map(fn: (r) => ({_value: r._value, _time: r._time, _field: "min"}))
  |> keep(columns: outputColumns)
  |> aggregateWindow(every: v.windowPeriod, fn: min, createEmpty: false)
  |> yield(name: "min")

packetLossRateRawQuery = from(bucket: bucket_name)
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "ping")
  |> filter(fn: (r) => r["_field"] == "percent_packet_loss")
  |> filter(fn: (r) => r["host"] == src_hostname)
  |> filter(fn: (r) => r["url"] == dst_url)

packetLossRateRet = packetLossRateRawQuery
  |> map(fn: (r) => ({_value: r._value, _time: r._time, _field: "packet_loss%"}))
  |> keep(columns: outputColumns)
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "packet_loss_rate")
